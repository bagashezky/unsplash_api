import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../controller/image/collection_controller.dart';
import 'page/home/home_page.dart';

class AppWidget extends StatelessWidget {
  const AppWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => CollectionController(),
      child: MaterialApp(
        title: 'Bagas',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blueGrey,
        ),
        home: const HomePage(
          title: 'Bagas',
        ),
      ),
    );
  }
}
